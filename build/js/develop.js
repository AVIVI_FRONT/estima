function dataDropdown(){
    var btn = '[data-dropdown-btn]';
    if(btn.length){
        $(document).on('click', btn, clickHandler);
    }else{
        $(document).off('click', btn, clickHandler);
    }
    function clickHandler(){
        var id = $(this).attr('data-dropdown-btn');
        if(!$(this).hasClass('active')){
            openDD(id);
        }else{
            closeDD(id);
        }
    }
    var close = '[data-dropdown-close]';
    if(close.length){
        $(document).on('click', close, closeHandler);
    }else{
        $(document).off('click', close, closeHandler);
    }
    function closeHandler(){
        var id = $(this).attr('data-dropdown-close');
        if(!$(this).hasClass('active')){
            closeDD(id);
        }
    }
}
function openDD(id) {

    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.addClass('active');
    box.addClass('active');
}
function closeDD(id) {
    var box = $('[data-dropdown-box='+id+']');
    var btn = $('[data-dropdown-btn='+id+']');
    btn.removeClass('active');
    box.removeClass('active');
}
function oneScreenScroll() {
    var settings = {
        animationTime: 600,
        infinite: false,
        quietPeriod: 200,
        enableScroll: true
    };
    var lastAnimation;
    var slider = $('.js-slider-screen');
    if($('html').hasClass('desktop')){
        if (slider.length > 0 ){
            window.scrollTop = 0;
            var screens = $('.js-slider-screen > *');
            screens.addClass('tp-screen').eq(0).addClass('tp-screen-active');
            if(settings.enableScroll){
                buildScroll(screens.length);
            }
            $(document).bind('mousewheel DOMMouseScroll', ishandler);
        }
    }else{
        $(document).unbind('mousewheel DOMMouseScroll', ishandler);
    }
    $(document).on('click','.js-slider-screen-downbutton', moveNext);
    function ishandler(event) {
            var delta = event.originalEvent.wheelDelta || -event.originalEvent.detail;
            initScroll(event, delta);
    }
    function initScroll(event, delta) {
        var active = document.getElementsByClassName('tp-screen-active')[0];
        var deltaOfInterest = delta,
            timeNow = new Date().getTime();
        if(timeNow - lastAnimation < settings.quietPeriod + settings.animationTime) {
            return;
        }
        if(window.innerWidth > document.body.clientWidth){
            if(window.scrollY < active.offsetHeight - window.innerHeight && deltaOfInterest < 0) return;
            if(window.scrollY > 0 && deltaOfInterest > 0) return;
        }
        if (deltaOfInterest < 0) {
            moveNext();
        } else {
            movePrev();
        }
        lastAnimation = timeNow;
    }
    function moveNext() {
        var el = $('.tp-screen-active');
        var next = el.next();
        if(next.length > 0){
            el.removeClass('tp-screen-active');
            next.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }else if(settings.infinite) {
            next = $('.tp-screen:first-child');
            el.removeClass('tp-screen-active');
            next.addClass('tp-screen-active');
            setTimeout(  afterShow ,100);
        }
    }
    function movePrev() {
        var el = $('.tp-screen-active');
        var prev =   el.prev();
        if(prev.length > 0){
            el.removeClass('tp-screen-active');
            prev.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }else if(settings.infinite) {
            prev = $('.tp-screen:last-child');
            el.removeClass('tp-screen-active');
            prev.addClass('tp-screen-active');
            setTimeout(  afterShow , 100);
        }
    }
    function afterShow() {
        if ($('.tp-screen-active').hasClass('inverse-header')){
            $('header').addClass('inverse');
        } else {
            $('header').removeClass('inverse');
        }
        $('.tp-screen__scroll').css('top', ((window.innerHeight/$('.js-slider-screen > *').length) * $('.tp-screen-active').index()).toFixed()+'px');
    }
    function buildScroll(count) {
        var h = window.innerHeight;
        var scroll = document.createElement('div');
        scroll.classList.add('tp-screen__bar');
        scroll.innerHTML = '<div class="tp-screen__scroll" style="top:0; height: ' + (h/count).toFixed() + 'px;"></div>';
        document.body.append(scroll)

    }
}
var prevArrow = '<button type="button" class="slider__arrow slider__prev"><svg><use xlink:href="#left-arrow"></use></svg></button>',
    nextArrow = '<button type="button" class="slider__arrow slider__next"><svg><use xlink:href="#right-arrow"></use></svg></button>';
function slidersInitialization() {
    var el = $('.js-slider4el');
    if(el.length > 0){
        el.slick({
            slidesToShow:4,
            slidesToScroll:4,
            dots: true,
            infinite: false,
            arrows: !$('html').hasClass('mobile'),
            prevArrow: prevArrow,
            nextArrow: nextArrow,
            responsive: [
                {breakpoint: 1100, settings:{slidesToShow: 3,slidesToScroll: 3}},
                {breakpoint: 850, settings:{slidesToShow: 2, slidesToScroll: 2}},
                {breakpoint: 600, settings:{slidesToShow: 1, slidesToScroll: 1}},
            ]

        });
    }
    var el2 = $('.js-first__slider');
    if(el2.length > 0){

        el2.slick({
            slidesToShow:1,
            dots: false,
            arrows: false,
            autoplaySpeed: 5000,
            autoplay: true,
        });
    }
    el2.on('afterChange', function(event, slick, currentSlide){
        $('.first__title').eq(currentSlide).addClass('active');
    });
    el2.on('beforeChange', function(event, slick, currentSlide){
        $('.first__title').removeClass('active');
    });

    var el3 = $('.js-material__slider');
    if(el3.length > 0){

        el3.slick({
            slidesToShow:3,
            slidesToScroll:1,
            rows: 2,
            infinite: false,
            dots: true,
            arrows: !$('html').hasClass('mobile'),
            prevArrow: prevArrow,
            nextArrow: nextArrow,
            responsive: [
                {  breakpoint: 1150, settings: {  slidesToShow: 2 } },
                {  breakpoint: 1024, settings: {  slidesToShow: 4 } },
                {  breakpoint: 810, settings: {  slidesToShow: 3  } },
                {  breakpoint: 610, settings: {  slidesToShow: 2  } },
            ]
        });
    }
    var el4 = $('.js-photomat');
    if(el4.length > 0){
        el4.slick({
            infinite: false,
            slidesToShow:1,
            slidesToScroll:1,
            dots: true,
            arrows: !$('html').hasClass('mobile'),
            prevArrow: prevArrow,
            nextArrow: nextArrow,
        });
    }
    var el5 = $('.js-filtered__slider');
    if(el5.length > 0){
        el5.each(function () {
            var slider = $(this);
            var box = $(this).closest('.filtered');
            var filter = box.find('.filtered__sorter-btn');
            var options;
            switch (slider.data('active')){
                case 'info':
                    options = {
                        infinite: false,
                        slidesToShow:4,
                        slidesToScroll:1,
                        dots: true,
                        arrows: !$('html').hasClass('mobile'),
                        prevArrow: prevArrow,
                        nextArrow: nextArrow,
                        responsive: [
                            {  breakpoint: 992, settings: {  slidesToShow: 3 } },
                            {  breakpoint: 650, settings: {  slidesToShow: 2 } },
                            {  breakpoint: 450, settings: {  slidesToShow: 1 } },
                        ]
                    }
                    break;
                case 'video':
                case 'pano':
                    options = {
                        infinite: false,
                        slidesToShow:5,
                        slidesToScroll:1,
                        dots: true,
                        arrows: !$('html').hasClass('mobile'),
                        prevArrow: prevArrow,
                        nextArrow: nextArrow,
                        responsive: [
                            {  breakpoint: 992, settings: {  slidesToShow: 4 } },
                            {  breakpoint: 650, settings: {  slidesToShow: 3 } },
                            {  breakpoint: 450, settings: {  slidesToShow: 2 } },
                        ]
                    }
                    break;
                default:
                    options = {
                        infinite: false,
                        slidesToShow:6,
                        slidesToScroll:1,
                        dots: true,
                        arrows: !$('html').hasClass('mobile'),
                        prevArrow: prevArrow,
                        nextArrow: nextArrow,
                        responsive: [
                            {  breakpoint: 992, settings: {  slidesToShow: 4 } },
                            {  breakpoint: 650, settings: {  slidesToShow: 3 } },
                            {  breakpoint: 450, settings: {  slidesToShow: 2 } },
                        ]
                    }
                    break;
            }
            slider.slick(options);
            filter.click(function () {
                filter.removeClass('active');
                $(this).addClass('active');
                var id = $(this).data('filter');
                if(id === 'info'){
                    slider.slick('slickSetOption','slidesToShow', 4, true);
                    slider.slick('slickSetOption','responsive', [
                        {  breakpoint: 992, settings: {  slidesToShow: 3 } },
                        {  breakpoint: 650, settings: {  slidesToShow: 2 } },
                        {  breakpoint: 450, settings: {  slidesToShow: 1 } },
                    ], true);
                }
                if(id === 'pano' || id === 'video'){
                    slider.slick('slickSetOption','slidesToShow', 5, true);
                    slider.slick('slickSetOption','responsive', [
                        {  breakpoint: 992, settings: {  slidesToShow: 4 } },
                        {  breakpoint: 650, settings: {  slidesToShow: 3 } },
                        {  breakpoint: 450, settings: {  slidesToShow: 2 } },
                    ], true);
                }
                if(id.length > 0){
                    slider.slick('slickUnfilter');
                    slider.slick('slickFilter', function () {
                        return $(this).data('filter') === id;
                    });


                }else{
                    slider.slick('slickUnfilter');
                }
            });
            if(box.find('.filtered__sorter-btn.active').length > 0){
                box.find('.filtered__sorter-btn.active').trigger('click');
            }
        });

    }
    var el6 = $('.js-card__gal-main');
    var el7 = $('.js-card__gal-nav');
    if(el6.length > 0){
        el6.slick({
            slidesToShow:1,
            dots: false,
            arrows: false,
            infinite: false,
        });
        el7.slick({
            slidesToShow:4,
            dots: false,
            arrows: false,
            infinite: false,
            focusOnSelect: true,
            asNavFor: '.js-card__gal-main',
            responsive: [
                {  breakpoint: 1024, settings: {  slidesToShow: 3 } },
                {  breakpoint: 767, settings: {  slidesToShow: 17 } },
                {  breakpoint: 727, settings: {  slidesToShow: 16 } },
                {  breakpoint: 687, settings: {  slidesToShow: 15 } },
                {  breakpoint: 647, settings: {  slidesToShow: 14 } },
                {  breakpoint: 607, settings: {  slidesToShow: 13 } },
                {  breakpoint: 567, settings: {  slidesToShow: 12 } },
                {  breakpoint: 527, settings: {  slidesToShow: 11 } },
                {  breakpoint: 487, settings: {  slidesToShow: 10 } },
                {  breakpoint: 400, settings: {  slidesToShow: 9 } },
                {  breakpoint: 360, settings: {  slidesToShow: 8 } },
                {  breakpoint: 330, settings: {  slidesToShow: 7 } },
            ]
        });
        $(document).on('click', '.js-card__next', function () {
            el7.slick('slickNext');
        });
        $(document).on('click', '.js-card__prev', function () {
            el7.slick('slickPrev');
        })
        el7.on('afterChange', function(event, slick, currentSlide){
            $('.card__arrow').removeClass('disabled');
            if(currentSlide == 0) $('.card__prev').addClass('disabled');
            if(currentSlide == slick.slideCount - 1) $('.card__next').addClass('disabled');
        });
    }
    var el8 = $('.js-article__slider');
    if(el8.length > 0){
        el8.slick({
            slidesToShow:1,
            slidesToScroll:1,
            infinite: false,
            dots: true,
            arrows: !$('html').hasClass('mobile'),
            prevArrow: '<button type="button" class="card__arrow card__prev"><svg><use xlink:href="#filter-arrow"></use></svg></button>',
            nextArrow: '<button type="button" class="card__arrow card__next"><svg><use xlink:href="#filter-arrow"></use></svg></button>',


        });
    }
}
function headerInvertor() {
    var els =  $('.inverse-header');
    for (var i = 0 ; i < els.length; i++){
        var ws = $(window).scrollTop();
        var res = ws >= $(els[i]).offset().top && ws < ($(els[i]).offset().top + $(els[i]).outerHeight() - $('header').outerHeight()) && !$('.tp-screen-active').length;
        if(res){
            $('header').addClass('inverse');
        }else{
            $('header').removeClass('inverse');
        }
    }

}
function mapTooltip(){
    var el = '[data-text]';
    if($(el).length){
        $(document).on('mouseover', el, function (e) {
            $('.map__tooltip').remove();
            var text = $(this).data('text');
            var div = document.createElement('div');
            div.classList.add('map__tooltip');

            div.innerText = text;
            div.style.left = e.pageX + 'px';
            div.style.top = e.pageY + 'px';
            if (text.length > 0) $('body').append(div);
        });
        $(document).on('mouseout', el, function () {
            $('.map__tooltip').remove();
        });
    }
}
function cardTooltip(){
    var el = '.card__param';
    if($(el).length){
        $(document).on('mouseover', el, function (e) {
            $('.map__tooltip').remove();
            var title = $(this).data('title');
            var text = $(this).data('tooltip');
            var div = document.createElement('div');
            div.classList.add('card__tooltip');
            div.innerHTML = '<b>' + title + '</b><p>' + text + '</p>';
            div.style.left = (e.pageX + 220 >  window.innerWidth ? e.pageX - (e.pageX + 220 -  window.innerWidth): e.pageX) + 'px';
            div.style.top = e.pageY + 'px';
            if (text.length > 0) {
                $('body').append(div);


            }

        });
        $(document).on('mouseout', el, function () {
            $('.card__tooltip').remove();
        });
    }
}
function resetFilter(el) {
    $(el).closest('.filter__item').find('input').each(function () {
        $(this).prop('checked', false).removeAttr('checked');
    })
}
function goTo(dest) {
    $('html, body').stop().animate({scrollTop:dest > 0 ? dest - $('header').height() : dest},800);
}
function onFocusInput() {
    var el = $('.gmap__input--start input');
    el.focus(function () {
        $('.gmap__search').addClass('show');
    });
    $(document).on('click', '.gmap__clicks--close', function () {
        $('.gmap__search').removeClass('show');
    });
}
$(document).ready(function () {
    dataDropdown();
    slidersInitialization();
    oneScreenScroll();
    mapTooltip();
    onFocusInput();
    cardTooltip();
    setTimeout(function () {
        $('.preloader').addClass('hide');
        setTimeout(function () {
            $('.preloader').remove();
        }, 500);
    }, 500);

    if($('.catalog-nav__top').length > 0){
        $('.catalog-nav__top .mbox').scrollLeft($('.catalog-nav__top .active').offset().left);
    }
});
var scroller = debounce(function () {
    headerInvertor();
}, 50);
window.addEventListener('scroll', scroller);
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};